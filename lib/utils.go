package lib

import (
	"bytes"
	"crypto/md5"
	"encoding/base64"
	"fmt"
	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"image/png"
)

//GenerateImageB64s 生成图片
func GenerateImageB64s(url string, width, height int) (*string, error) {
	code, err := qr.Encode(url, qr.L, qr.Unicode)
	if err != nil {
		return nil, err
	}

	code, err = barcode.Scale(code, width, height)
	if err != nil {
		return nil, err
	}
	var buf bytes.Buffer
	err = png.Encode(&buf, code)
	if err != nil {
		return nil, err
	}
	imgBase64Str := fmt.Sprintf("data:image/png;base64,%s", base64.StdEncoding.EncodeToString(buf.Bytes()))
	return &imgBase64Str, nil
}

func NewPwd(password string) string {
	data := md5.Sum([]byte(password))
	return fmt.Sprintf("%x", data)
}
