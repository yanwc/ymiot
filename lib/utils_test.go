package lib

import (
	"fmt"
	"testing"
)

func Test_GenerateImageB64s(t *testing.T) {
	b64s, err := GenerateImageB64s("http://www.baidu.com", 300, 300)
	if err != nil {
		t.Error(err)
	}

	fmt.Println(*b64s)
}
