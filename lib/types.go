package lib

import (
	"net/http"
)

func SetHeader(r *http.Request) {
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
}

type LoginResponse struct {
	Success      bool
	Message      string
	AcwTc        string
	AdminSession string
}

//RequestContext 请求上下文
type RequestContext struct {
	AcwTc        string
	AdminSession string
}

func (ctx *RequestContext) SetHttpCookie(r *http.Request) {
	r.AddCookie(&http.Cookie{Name: "acw_tc", Value: ctx.AcwTc})
	r.AddCookie(&http.Cookie{Name: "AdminSession", Value: ctx.AdminSession})
}

type QrLoadResponse struct {
	Success bool   `json:"Success"`
	Message string `json:"Message"`
	Code    string `json:"Code"`
	Data    struct {
		CouponSolutionDWZ            string `json:"CouponSolution_DWZ"`
		CouponSolutionQRCodeUrl      string `json:"CouponSolution_QRcodeUrl"`
		CouponSolutionSumCount       int    `json:"CouponSolution_SumCount"`
		CouponSolutionRemainingCount int    `json:"CouponSolution_RemainingCount"`
	} `json:"Data"`
}

type CreateQrCodeRequest struct {
	CouponSolutionId int `json:"CouponSolution_ID"`
	SumCount         int `json:"SumCount"`
}

type CreateQrCodeResponse struct {
	Success bool   `json:"Success"`
	Message string `json:"Message"`
	Data    struct {
		CouponSolutionSumCount       int    `json:"CouponSolution_SumCount"`
		CouponSolutionRemainingCount int    `json:"CouponSolution_RemainingCount"`
		CouponSolutionDWZ            string `json:"CouponSolution_DWZ"`
	} `json:"Data"`
}
