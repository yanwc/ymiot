package lib

import (
	"fmt"
	"net/http"
	"testing"
)

func Test_QRCodeCreate(t *testing.T) {
	r, err := QRCodeCreate(http.Client{}, CreateQrCodeRequest{
		CouponSolutionId: 299636,
		SumCount:         1,
	}, RequestContext{
		AcwTc:        "2f6a1fe716511931976331490ee8a2ec0e5640b8ba39b090efbe71ed645a9d",
		AdminSession: "CAr1LZ8q4NLiDELZUm%2FBmEenW8mEp3eSzQkPCYh6gQqdw2g6jldK1dy03EGJrM9i",
	})
	if err != nil {
		t.Error(err)
	}

	fmt.Printf("%+v\n", r)
}
